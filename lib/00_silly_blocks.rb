
def reverser
   yield.split(" ").reverse.join(" ").reverse
end

def adder(num=1)
  yield + num
end

def repeater(rep=1, &block)
  rep.times do block.call end
end

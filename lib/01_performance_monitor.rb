def measure(reps=1, &block)
  elapsed_time = []
  reps.times do
    now = Time.now
    block.call
    elapsed_time << Time.now - now
  end
  # return elapsed_time[0] if reps = 1
  # puts "full: #{elapsed_time} sum: #{elapsed_time.reduce(:+)} #{elapsed_time.length}  "
  (elapsed_time.reduce(:+)) / (elapsed_time.length.to_f)
end
